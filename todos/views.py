from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodolistForm, TodoItemForm

# Create your views here.
def todo_list_list (request): 
    todolist = TodoList.objects.all()
    context = { 
        "todo_list_list": todolist, 
    }
    return render(request, "todos/list.html", context)

def todo_list_detail (request, id): 
    tododetail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": tododetail, 
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request): 
    if request.method == "POST": 
        form = TodolistForm (request.POST)
        if form.is_valid(): 
            todo_list = form.save(False)
            todo_list.save() 
            return redirect ("todo_list_detail", id=todo_list.id)
    else: 
        form = TodolistForm()

    context = { 
          "form": form, 
        }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id): 
    todo_list_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST": 
        form = TodolistForm(request.POST, instance=todo_list_instance)
        if form.is_valid(): 
            form.save() 
            return redirect ("todo_list_detail", id=id)
    else: 
        form = TodolistForm(instance=todo_list_instance)

    context = { 
        "todo_list_object": todo_list_instance, 
        "form" : form, 
    }

    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id): 
    todo_list_instance = TodoList.objects.get(id=id)
    if request.method == "POST": 
        todo_list_instance.delete() 
        return redirect("todo_list_list")
    
    return render(request, "todos/delete.html")

def todo_item_create(request): 
    if request.method == "POST": 
        form = TodoItemForm (request.POST)
        if form.is_valid(): 
            todo_item = form.save(False)
            todo_item.save() 
            return redirect("todo_list_detail", id=todo_item.list.id)
    
    else: 
        form = TodoItemForm() 
    
    context = { 
        "form": form, 
    }

    return render(request, "todos/create_item.html", context)

def todo_item_update(request, id): 
    todo_item_instance = get_object_or_404(TodoItem, id=id)
    if request.method == "POST": 
        form = TodoItemForm(request.POST, instance=todo_item_instance)
        if form.is_valid(): 
            form.save() 
            return redirect ("todo_list_detail", id=todo_item_instance.list.id)
    else: 
        form = TodoItemForm(instance=todo_item_instance)

    context = { 
        "todo_item_object": todo_item_instance, 
        "form" : form, 
    }

    return render(request, "todos/edit_item.html", context)